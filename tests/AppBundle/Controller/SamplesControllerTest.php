<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/6/18
 * Time: 5:56 PM
 */

namespace Tests\App\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SamplesControllerTest extends WebTestCase
{
    public function testShowHelloWorld()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/hello');
        $this->assertGreaterThan(
            1,
            $crawler->filter('div:contains("hello worlds")')->count()
        );
    }

    public function testShowFormAction()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "form-test");
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );

        //get the form
        $form = $crawler->selectButton('submit')->form();
        $form['txt-firstname'] = "samples";
        //submit the form
        $crawler = $client->submit($form);

    }

    /**
     * test add post form
     */
    public function testAddPost()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', 'add-post');
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        //get the form
        $form = $crawler->selectButton('submit')->form();
        $i = rand(1000, 3000);
        $form['post[title]'] = 'unit test ' . $i;
        $form['post[status]'] = 1;
        $form['post[content]'] = 'unit test content ' . $i;
        $crawler = $client->submit($form);

        //test click me link
        $link = $crawler
            ->filter('a:contains("Click me")')
            ->eq(0)
            ->link();
        $crawler = $client->click($link);
    }
}